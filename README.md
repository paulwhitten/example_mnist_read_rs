# example_mnist_read_rs

Example of reading mnist dataset in rust.

Prerequisite
- download and extract [mnist](http://yann.lecun.com/exdb/mnist/) or emnist

## scripts

### save_mnist_image_by_number

A script for extracting a sample from the mnist training set.

Use:

```
Outputs an mnist sample to a *.png file.
The file will be named x_d_i, where x is the zero
based MNIST sample number, d is the labeled digit,
and i is the instance number of the digit in MNIST.

Usage: save_mnist_image_by_number [OPTIONS] --mnist-path <MNIST_PATH>

Options:
  -m, --mnist-path <MNIST_PATH>  Location of mnist files [env: MNIST_PATH=]
  -n, --number <NUMBER>          Zero based number of the desired MNIST sample [default: 0]
  -o, --output <OUTPUT>          Output folder [default: ./]
  -h, --help                     Print help
  -V, --version                  Print version
```

Example:

```sh
cargo run --bin save_mnist_image_by_number -- -m ~/mnist/downloads -n 49001
```

### save_image_by_number

A script for extracting a sample from a mnist-like database.

Use:

```
Outputs a sample from an mnist-like database to
a *.png file. The file will be named x_d_i, where
x is the zero based sample number, d is the
labeled digit, and i is the instance number of the
digit in the database.

Usage: save_image_by_number [OPTIONS] --image-file <IMAGE_FILE> --label-file <LABEL_FILE>

Options:
  -i, --image-file <IMAGE_FILE>  The image file [env: IMAGE_FILE=]
  -l, --label-file <LABEL_FILE>  The label file [env: LABEL_FILE=]
  -n, --number <NUMBER>          Zero based number of the desired sample [default: 0]
  -c, --column-major             Colum major encoding like EMNIST.  MNIST coding is row
  -o, --output <OUTPUT>          Output folder [default: ./]
  -h, --help                     Print help
  -V, --version                  Print version
  ```

Example:

```sh
cargo run --bin save_image_by_number -- -i ~/emnist/downloads/emnist-balanced-train-images-idx3-ubyte -l ~/emnist/downloads/emnist-balanced-train-labels-idx1-ubyte -n 4 -c
```

### axum_mnist_web_api

A web API used to interact with mnist-like datasets.

Edit the file `config.yml` file to indicate the datasets adn their locations.
The example shows the mnist test and training sets, which are in row major format
and the emnist byclass training set which is column major format.

Use:

```sh
cargo run --bin axum_mnist_web_api
```

The web server will be started listening to port 3000.

Interact with the datsets using the following rest calls:

- GET - `http://localhost:3000/` - to obtain the hosted dataset keys separated
  by commas.  If no datasets were found, a 404 error will be returned.

  For example, using httpie:
  ```
  % http GET http://localhost:3000/                          
  HTTP/1.1 200 OK
  content-length: 41
  content-type: text/plain; charset=utf-8
  date: Mon, 18 Sep 2023 20:35:44 GMT

  emnist_training,mnist_test,mnist_training
  ```

- GET - `http://localhost:3000/<dataset_name>/images` - to obtain the comma separated list of image indexes available.

  For example, using httpie, run `http GET http://localhost:3000/mnist_test/images` to obtain a long list of valid indices 0 through 9999.

- GET - `http://localhost:3000/<dataset_name>/images/<index_number>?fmt=<desired_format>` - to obtain data on the specified image in the desired format format.  Not specifying the format query will result in the rust debug format. Valid
  formats include:
  - `debug` - rust debug format, the default
  - `json` - jacvascript object notation
  - `png` - an image
  - `bytes` - base64 encoded byte array
  - `csv` - a csv representation of the image
  - `ascii` - an ascii rendering

  For example
  
  ```sh
  % http GET "http://localhost:3000/mnist_test/images/3?format=ascii"
  HTTP/1.1 200 OK
  content-length: 1596
  content-type: text/plain; charset=utf-8
  date: Mon, 18 Sep 2023 20:21:04 GMT

  --------------------------------------------------------
  --------------------------------------------------------
  --------------------------------------------------------
  --------------------------------------------------------
  --------------------------**%%##------------------------
  --------------------------%%%%%%**----------------------
  ------------------------##%%%%%%**----------------------
  --------------------**##%%%%%%%%##**==------------------
  --------------------%%%%%%%%%%%%%%%%%%------------------
  ------------------##%%%%%%%%%%%%%%%%%%%%----------------
  ----------------==%%%%%%%%%%**====**%%%%**--------------
  ----------------%%%%%%%%%%**--------%%%%%%**------------
  ----------------%%%%%%##------------**%%%%%%------------
  ----------------%%%%##----------------##%%%%------------
  ----------------%%%%------------------##%%%%##----------
  --------------**%%%%----------------**%%%%%%------------
  --------------%%%%%%--------------==%%%%%%%%------------
  --------------%%%%%%------------**%%%%%%%%==------------
  --------------%%%%%%----------##%%%%%%%%##--------------
  --------------%%%%%%====%%%%%%%%%%%%%%%%----------------
  --------------**%%%%%%%%%%%%%%%%%%%%%%**----------------
  ----------------%%%%%%%%%%%%%%%%%%##--------------------
  ----------------==**%%%%%%%%%%%%**----------------------
  --------------------==##%%##====------------------------
  --------------------------------------------------------
  --------------------------------------------------------
  --------------------------------------------------------
  --------------------------------------------------------
  ```
