use clap::Parser;
use image_dataset::{ImageDataset, ImageEncoding};
use std::{path::Path, time::Instant};
use tracing::{debug, Level}; // error, info, span, warn,
use tracing_subscriber::FmtSubscriber;

// TODO: need to figure out how to transform digit to character

#[derive(Parser, Debug)]
#[command(name = "read_mnist")]
#[command(author = "Paul Whitten <pcw@case.edu>")]
#[command(version = "1.0")]
#[command(about = "Outputs a sample from an mnist-like database to
a *.png file. The file will be named x_d_i, where
x is the zero based sample number, d is the
labeled digit, and i is the instance number of the
digit in the database.", long_about = None)]
struct Args {
    /// The image file
    #[arg(short, long, env)]
    image_file: String,

    /// The label file
    #[arg(short, long, env)]
    label_file: String,

    /// Zero based number of the desired sample
    #[arg(short, long, default_value_t = 0)]
    number: usize,

    /// Column-major encoding like EMNIST.  MNIST encoding is row-major
    #[arg(short, long, default_value_t = false)]
    column_major: bool,

    /// Output folder
    #[arg(short, long, default_value_t = String::from("./"))]
    output: String,
}

fn main() {
    let _subscriber = FmtSubscriber::builder().with_max_level(Level::TRACE).init();

    let args = Args::parse();

    debug!("Args: {:?}", args);

    let t = Instant::now();

    let id = ImageDataset::from_file(
        &args.image_file,
        &args.label_file,
        if args.column_major {
            ImageEncoding::ColumnMajor
        } else {
            ImageEncoding::RowMajor
        },
    )
    .expect("Problem getting the dataset");

    let image_item = id
        .get_image(args.number)
        .expect("Problem getting the image");

    let output_file = format!(
        "{number}_{label}_{count}.png",
        number = args.number,
        label = image_item.label,
        count = image_item.item_instance
    );
    image_item
        .image
        .save(Path::new(&args.output).join(output_file))
        .expect("Problem writing image.");

    debug!("Time elapsed: {:?}", t.elapsed());
}
