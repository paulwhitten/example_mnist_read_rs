//mod image_dataset;

use clap::Parser;
use image_dataset::{ImageDataset, ImageEncoding};
use std::{path::Path, time::Instant};
use tracing::{debug, Level}; // error, info, span, warn,
use tracing_subscriber::FmtSubscriber;

#[derive(Parser, Debug)]
#[command(name = "average of pixels")]
#[command(author = "Paul Whitten <pcw@case.edu>")]
#[command(version = "1.0")]
#[command(about = "Takes the average of pixels.", long_about = None)]
struct Args {
    /// The path to the image file
    #[arg(short, long, env)]
    image_path: String,

    /// The path to the label file
    #[arg(short, long, env)]
    label_path: String,

    /// Output folder
    #[arg(short, long, default_value_t = String::from("./"))]
    output: String,
}

fn main() {
    let _subscriber = FmtSubscriber::builder().with_max_level(Level::TRACE).init();

    let args = Args::parse();

    debug!("Args: {:?}", args);

    let t = Instant::now();

    let image = Path::new(&args.image_path);
    let label = Path::new(&args.label_path);
    let id = ImageDataset::from_file(image, label, ImageEncoding::RowMajor)
        .expect("Problem getting the dataset");

    let label_min = id.labels.iter().min().unwrap();
    let label_max = id.labels.iter().max().unwrap();

    // Create the combined dataset.
    // This will be the sum of pixel values / max(pixel value) * 255
    let mut combined_set = ImageDataset::new();
    combined_set.rows = id.rows;
    combined_set.columns = id.columns;
    combined_set.encoding = id.encoding;

    // Create the average dataset.
    // This will be the sum of pixel values / number of images
    let mut average_set = ImageDataset::new();
    average_set.rows = id.rows;
    average_set.columns = id.columns;
    average_set.encoding = id.encoding;

    // iterate through each label
    for label in *label_min..*label_max + 1 {
        // grab all images with the current label (label)
        let im_data = id.get_all_from_label(label);
        // image for "combining" all images with eh same label
        let mut combined = Vec::new();
        let mut max = 0;
        for i in 0..id.rows * id.columns {
            let mut acum: usize = 0; // maintain the max pixel  value
            for sample in &im_data {
                acum += sample[i] as usize;
            }
            if max < acum {
                max = acum;
            }
            combined.push(acum);
        }

        let mut combined_byte_data: Vec<u8> = Vec::new();
        let mut average_byte_data: Vec<u8> = Vec::new();
        for px in combined.iter_mut() {
            // adjust average according to number of images
            average_byte_data.push((*px / im_data.len()) as u8);
            // adjust combined pixel value according to the max pixel value
            *px = ((*px as f32 / max as f32) * 255.0f32) as usize;
            combined_byte_data.push(*px as u8);
        }

        let combined_gray_img = id.get_image_from_data(&combined_byte_data).unwrap();
        let average_gray_img = id.get_image_from_data(&average_byte_data).unwrap();

        let combined_output_file = format!("{}/combined_{label}.png", &args.output);
        let average_output_file = format!("{}/average_{label}.png", &args.output);

        // output images
        combined_gray_img
            .save(combined_output_file)
            .expect("Problem writing image.");

        average_gray_img
            .save(average_output_file)
            .expect("Problem writing image.");

        // add images to the datasets
        combined_set.add_image(&combined_byte_data, label);
        average_set.add_image(&combined_byte_data, label);
    }

    // save the data sets to file
    combined_set
        .save(
            format!("{}/combined_image", &args.output),
            format!("{}/combined_label", &args.output),
        )
        .unwrap();
    average_set
        .save(
            format!("{}/average_image", &args.output),
            format!("{}/average_label", &args.output),
        )
        .unwrap();

    debug!("Time elapsed: {:?}", t.elapsed());
}
