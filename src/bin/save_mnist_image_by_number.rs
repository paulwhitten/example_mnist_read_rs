//mod image_dataset;

use clap::Parser;
use image_dataset::{ImageDataset, ImageEncoding};
use std::{path::Path, time::Instant};
use tracing::{debug, Level}; // error, info, span, warn,
use tracing_subscriber::FmtSubscriber;

#[derive(Parser, Debug)]
#[command(name = "read_mnist")]
#[command(author = "Paul Whitten <pcw@case.edu>")]
#[command(version = "1.0")]
#[command(about = "Outputs an mnist sample to a *.png file.
The file will be named x_d_i, where x is the zero
based MNIST sample number, d is the labeled digit,
and i is the instance number of the digit in MNIST.", long_about = None)]
struct Args {
    /// Location of mnist files
    #[arg(short, long, env)]
    mnist_path: String,

    /// Zero based number of the desired MNIST sample
    #[arg(short, long, default_value_t = 0)]
    number: usize,

    /// Output folder
    #[arg(short, long, default_value_t = String::from("./"))]
    output: String,
}

fn main() {
    let _subscriber = FmtSubscriber::builder().with_max_level(Level::TRACE).init();

    let args = Args::parse();

    debug!("Args: {:?}", args);

    let t = Instant::now();

    let base = Path::new(&args.mnist_path);
    let image = base.join("train-images-idx3-ubyte");
    let label = base.join("train-labels-idx1-ubyte");
    let id = ImageDataset::from_file(image, label, ImageEncoding::RowMajor)
        .expect("Problem getting the dataset");

    let image_item = id
        .get_image(args.number)
        .expect("Problem getting the image");

    let output_file = format!(
        "{number}_{label}_{count}.png",
        number = args.number,
        label = image_item.label,
        count = image_item.item_instance
    );
    image_item
        .image
        .save(Path::new(&args.output).join(output_file))
        .expect("Problem writing image.");

    debug!("Time elapsed: {:?}", t.elapsed());
}
