use axum::{
    body::{boxed, Body},
    extract::{Path, Query, State},
    http::{header, StatusCode},
    response::Response,
    routing::get,
    Router,
};
use image_dataset::{ImageDataset, ImageEncoding};
use itertools::Itertools;
use serde::{Deserialize, Serialize};
use snafu::prelude::*;
use std::{
    collections::BTreeMap,
    io::{BufWriter, Cursor},
    net::SocketAddr,
    sync::Arc,
};
use tracing::{debug, error, Level};
use tracing_subscriber::FmtSubscriber;

#[derive(Debug, Snafu)]
pub enum ConfigError {
    #[snafu(display("Problem reading config file"))]
    ReadConfig { source: std::io::Error },
    #[snafu(display("Problem interpreting config yaml"))]
    ParsingConfig { source: serde_yaml::Error },
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
struct DataSet {
    image_file: String,
    label_file: String,
    encoding: ImageEncoding,
}

// https://github.com/tokio-rs/axum/blob/v0.6.x/examples/readme/src/main.rs

// shared state
// https://github.com/tokio-rs/axum/blob/v0.6.x/examples/key-value-store/src/main.rs
// https://wiki.enablingpersonalizedinterventions.nl/docs/axum/index.html#using-closure-captures

#[derive(Debug)]
struct ApiState {
    pub image_db_map: BTreeMap<String, ImageDataset>,
}

impl ApiState {
    pub fn new() -> ApiState {
        ApiState {
            image_db_map: BTreeMap::new(),
        }
    }
}

type SharedState = Arc<ApiState>;

// Query https://github.com/tokio-rs/axum/blob/v0.6.x/examples/query-params-with-empty-strings/src/main.rs
// consider handling
// BYTES, CSV, JSON, PNG, DBG
#[derive(Debug, Deserialize, Serialize, PartialEq)]
//#[serde(rename_all = "lowercase")] //pascal case? alternative is using alias
enum ImageFormat {
    #[serde(alias = "json")]
    JSON,
    #[serde(alias = "csv")]
    CSV, // base64 encoded
    #[serde(alias = "BYTE", alias = "bytes", alias = "byte")]
    BYTES,
    #[serde(alias = "png")]
    PNG,
    #[serde(alias = "DBG", alias = "debug", alias = "dbg")]
    DEBUG,
    #[serde(alias = "ascii")]
    ASCII,
}

#[derive(Debug, Deserialize)]
struct ImageFormatParams {
    format: Option<ImageFormat>,
}

fn load_config(
    path: impl AsRef<std::path::Path>,
) -> Result<BTreeMap<String, DataSet>, ConfigError> {
    let yaml = std::fs::read_to_string(path).context(ReadConfigSnafu)?;
    let yml = serde_yaml::from_str(&yaml).context(ParsingConfigSnafu)?;
    Ok(yml)
}

#[tokio::main]
async fn main() {
    // initialize tracing
    let _subscriber = FmtSubscriber::builder().with_max_level(Level::TRACE).init();

    // load the datasets in the config
    let cfg = match load_config("config.yml") {
        Ok(config) => config,
        Err(e) => {
            error!("Problem loading config, {}", e);
            std::process::exit(1);
        }
    };
    let mut api_state: ApiState = ApiState::new();
    for key in cfg.keys() {
        let ds = cfg.get(key).unwrap();
        let image_db = match ImageDataset::from_file(&ds.image_file, &ds.label_file, ds.encoding) {
            Ok(dataset) => {
                debug!("Opened {} dataset with encoding {:?}", key, ds.encoding);
                dataset
            }
            Err(e) => {
                error!("Problem opening dataset {}, {}", key, e);
                std::process::exit(1);
            }
        };
        api_state.image_db_map.insert(key.to_owned(), image_db);
    }
    let shared_state = Arc::new(api_state);

    // build the application with a route
    let app = Router::new()
        // `GET /` goes to `root` handler
        .route("/", get(root))
        // `GET /mnist/images` goes to `get_images` handler
        .route("/:db_key/images", get(get_images))
        // `GET /:db_key/images/:image` goes to `get_images` handler
        .route("/:db_key/images/:image", get(get_image))
        .with_state(Arc::clone(&shared_state));

    // run our app with hyper
    // `axum::Server` is a re-export of `hyper::Server`
    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    debug!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

// basic handler that responds with the dataset keys
async fn root(State(state): State<SharedState>) -> Result<Response, StatusCode> {
    debug!("In root. Enumerating the datasets.");
    // try accessing shared state
    let mut dbs: Vec<String> = Vec::new();
    for key in state.image_db_map.keys() {
        debug!(
            "db: {}, num items: {}",
            key,
            state.image_db_map.get(key).unwrap().num_items
        );
        dbs.push(key.to_owned());
    }

    if dbs.len() == 0 {
        return Err(StatusCode::NOT_FOUND);
    }

    let csv_info = itertools::join(dbs, ",");
    Ok(Response::builder()
        .header(header::CONTENT_TYPE, "text/plain; charset=utf-8")
        .body(boxed(Body::from(csv_info)))
        .unwrap())
}

// handler to indicate the image indices that are available
// could make this simpler by just returning the count
async fn get_images(
    Path(db_key): Path<String>,
    State(state): State<SharedState>,
) -> Result<Response, StatusCode> {
    let mut image_indexes = String::new();

    match state.image_db_map.get(&db_key) {
        None => Err(StatusCode::NOT_FOUND),
        Some(db) => {
            let mut i: usize = 0;
            while i < db.num_items {
                // TODO make a public num method
                if i != 0 {
                    image_indexes += ", "
                }
                image_indexes += &i.to_string();
                i += 1;
            }
            Ok(Response::builder()
                .header(header::CONTENT_TYPE, "text/plain; charset=utf-8")
                .body(boxed(Body::from(image_indexes)))
                .unwrap())
        }
    }
}

// https://wiki.enablingpersonalizedinterventions.nl/docs/axum/index.html
// https://wiki.enablingpersonalizedinterventions.nl/docs/axum/struct.Router.html
/// Handle GET /:dataset_key/images/:image?format=fmt
async fn get_image(
    Path((db_key, image)): Path<(String, usize)>,
    State(state): State<SharedState>,
    Query(params): Query<ImageFormatParams>,
) -> Result<Response, StatusCode> {
    debug!(
        "In get_image(), db_key: {db_key}, image: {image}, params{:?}",
        params
    );

    let db = match state.image_db_map.get(&db_key) {
        Some(d) => d,
        None => {
            error!("db key, {db_key}, not found");
            return Err(StatusCode::NOT_FOUND);
        }
    };
    match db.get_image(image) {
        Err(e) => {
            error!("get_image({image}) gave an error: {e}");
            Err(StatusCode::NOT_FOUND)
        }
        Ok(image_item) => {
            debug!("Got {image}");
            debug!("Query: {:?}", params);

            match params.format {
                Some(ImageFormat::PNG) => {
                    // get the image bytes by writing to a vector
                    let mut buffer = BufWriter::new(Cursor::new(Vec::new()));
                    image_item
                        .image
                        .write_to(&mut buffer, image::ImageOutputFormat::Png)
                        .unwrap();
                    let bytes: Vec<u8> = buffer.into_inner().unwrap().into_inner();

                    // form the attachment filename we are sending
                    let attachment_header_value = format!(
                        "attachment; filename=\"{image}_{label}_{instance}.png\"",
                        label = image_item.label,
                        instance = image_item.item_instance
                    );

                    Ok(Response::builder()
                        .header(header::CONTENT_TYPE, "image/png")
                        .header(header::CONTENT_DISPOSITION, attachment_header_value)
                        .body(boxed(Body::from(bytes)))
                        .unwrap())
                }
                Some(ImageFormat::CSV) => {
                    let csv_info = itertools::join(image_item.image.iter(), ",");
                    Ok(Response::builder()
                        .header(header::CONTENT_TYPE, "text/plain; charset=utf-8")
                        //.header(header::CONTENT_TYPE, "text/csv") // downloads a file
                        .body(boxed(Body::from(csv_info)))
                        .unwrap())
                }
                Some(ImageFormat::BYTES) => {
                    let mut image: Vec<u8> = Vec::new();
                    for (_x, _y, pixel) in image_item.image.enumerate_pixels() {
                        image.push(pixel.0[0]);
                    }
                    Ok(Response::builder()
                        .header(header::CONTENT_TYPE, "text/plain; charset=ISO-8859-1")
                        //.header("Content-transfer-encoding:", "base64") // doe not like this
                        // TODO base64::encode() is deprecated.  holy smokes base64 seems over complicated.  look for another crate
                        .body(boxed(Body::from(base64::encode(&image))))
                        .unwrap())
                }
                Some(ImageFormat::JSON) => {
                    let image = (&image_item.image.iter().cloned().chunks(db.columns))
                        .into_iter()
                        .map(|chunk| chunk.map(|y| y).collect::<Vec<_>>())
                        .collect::<Vec<_>>();

                    let json_image = serde_json::to_string(&image).unwrap();
                    //Ok(Json(image)) // this doesn't work
                    Ok(Response::builder()
                        .header(header::CONTENT_TYPE, "application/json")
                        .body(boxed(Body::from(json_image)))
                        .unwrap())
                }
                Some(ImageFormat::ASCII) => {
                    let image = (&image_item.image.iter().cloned().chunks(db.columns))
                        .into_iter()
                        .map(|chunk| chunk.map(|y| y).collect::<Vec<_>>())
                        .collect::<Vec<_>>();
                    let intensity_map = vec!["-", "=", "*", "#", "%"];
                    let mut img_ascii = String::new();
                    for i in image {
                        for j in i {
                            let d = j / 52;
                            let px_val = intensity_map[d as usize];
                            img_ascii += &format!("{}{}", px_val, px_val);
                        }
                        img_ascii += "\n";
                    }
                    Ok(Response::builder()
                        .header(header::CONTENT_TYPE, "text/plain; charset=utf-8")
                        .body(boxed(Body::from(img_ascii)))
                        .unwrap())
                }
                _ => {
                    let dbg_image = format!("{:?}", image_item);

                    let response = Response::builder()
                        .header(header::CONTENT_TYPE, "text/plain; charset=utf-8")
                        .body(boxed(Body::from(dbg_image)))
                        .unwrap();
                    Ok(response)
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::DataSet;
    use image_dataset::ImageEncoding;
    use std::collections::BTreeMap;
    use tracing::Level;
    use tracing_subscriber::FmtSubscriber;

    // init tracing subscriber
    #[cfg(test)]
    #[ctor::ctor]
    fn init() {
        let _subscriber = FmtSubscriber::builder().with_max_level(Level::TRACE).init();
    }

    #[test]
    // not really a test.  just for setting up a simple config file
    fn output_config_file() {
        let mut map = BTreeMap::new();
        map.insert(
            "mnist_training",
            DataSet {
                image_file: "<add image file path>".to_owned(),
                label_file: "<add label file path>".to_owned(),
                encoding: ImageEncoding::RowMajor,
            },
        );
        map.insert(
            "mnist_test",
            DataSet {
                image_file: "<add image file path>".to_owned(),
                label_file: "<add label file path>".to_owned(),
                encoding: ImageEncoding::ColumnMajor,
            },
        );
        let yaml = serde_yaml::to_string(&map).expect("Problem serializing yaml");
        std::fs::write("test_config.yml", yaml).expect("Problem writing file.");
    }
}
