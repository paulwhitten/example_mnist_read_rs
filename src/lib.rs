use byteorder::{BigEndian, ReadBytesExt};
use image::{GrayImage, Luma};
use serde::{Deserialize, Serialize};
use snafu::prelude::*;
use std::{fs::File, io::Read, io::Write, path::Path};
use tracing::debug; // error, info, span, warn,

const TYPE_UNSIGNED_BYTE: u8 = 0x08;
const IMAGE_DIMENSIONS: u8 = 3;
const LABEL_DIMENSIONS: u8 = 1;

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("Problem opening file"))]
    OpenFile { source: std::io::Error },
    #[snafu(display("Problem reading magic number"))]
    ReadMagicNumber { source: std::io::Error },
    #[snafu(display("Problem reading type"))]
    ReadType { source: std::io::Error },
    #[snafu(display("Problem reading dimensions"))]
    ReadDimensions { source: std::io::Error },
    #[snafu(display("Problem reading dimension {dimension}"))]
    ReadDimension {
        source: std::io::Error,
        dimension: u8,
    },
    #[snafu(display("The {dimension}th dimension read was invalid: {dimension_size}"))]
    InvalidDimension {
        dimension: u8,
        dimension_size: usize,
    },
    #[snafu(display("Problem reading metadata"))]
    ReadMetadata { source: std::io::Error },
    #[snafu(display("Expected magic num {:#05x}, but it was {magic:#05x}", 0x0000))]
    InvalidMagic { magic: u32 },
    #[snafu(display("Expected type {TYPE_UNSIGNED_BYTE:#05x}, but it was {file_type:#02x}"))]
    InvalidType { file_type: u32 },
    #[snafu(display(
        "The expected image dimensions, {IMAGE_DIMENSIONS}, were invalid, {dimensions}"
    ))]
    InvalidImageDimensions { dimensions: u8 },
    #[snafu(display(
        "The expected label dimensions, {LABEL_DIMENSIONS}, were invalid, {dimensions}"
    ))]
    InvalidLabelDimensions { dimensions: u8 },
    #[snafu(display("No dimensions were found"))]
    ZeroDimensions,
    #[snafu(display(
        "File is of unexpected size, expected: {expected_size} actual: {actual_size}"
    ))]
    UnexpectedFileSize {
        expected_size: usize,
        actual_size: usize,
    },
    #[snafu(display("Read error"))]
    ReadData { source: std::io::Error },
    #[snafu(display("The data read expected {expected} but got {actual}"))]
    DataReadFailed { expected: usize, actual: usize },
    #[snafu(display("The index, {item_index} was out of bounds"))]
    IndexOutOfBounds { item_index: usize },
    #[snafu(display("Invalid dataset: image count: {num_images}, label count{num_labels}"))]
    InvalidDataset {
        num_images: usize,
        num_labels: usize,
    },
    #[snafu(display("Create file error"))]
    CreateFile { source: std::io::Error },
    #[snafu(display("Write error"))]
    WriteData { source: std::io::Error },
}

#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub enum ImageEncoding {
    RowMajor,
    ColumnMajor,
}

/// Represents an image dataset from a MNIST-like DB
#[derive(Debug)]
pub struct ImageDataset {
    /// y dimension
    pub rows: usize,
    /// x dimension
    pub columns: usize,
    /// the number of items in the dataset // TODO make a public num() method
    pub num_items: usize,
    /// the images
    image_buffer: Vec<u8>,
    /// the label for each image
    pub labels: Vec<u8>,
    /// row or column major encoding
    pub encoding: ImageEncoding,
    /// the per-class instance number of the item
    pub item_instances: Vec<usize>,
}

/// Represents an image result from the image dataset
#[derive(Debug)]
pub struct ImageItem {
    pub label: u8,
    pub item_index: usize,
    pub item_instance: usize,
    pub image: GrayImage,
}

/// Represents the contents of a database file
struct DbFile {
    dimensions: u8,    // the number of dimensions
    sizes: Vec<usize>, // the dimensions
    data: Vec<u8>,     // the data from the file
}

/// This function reads a database file
fn read_db_file(file_name: impl AsRef<Path>) -> Result<DbFile, Error> {
    // get the file header
    debug!("reading file {:?} header", file_name.as_ref());
    let mut f = File::open(file_name.as_ref()).context(OpenFileSnafu)?;
    let magic: u16 = f.read_u16::<BigEndian>().context(ReadMagicNumberSnafu)?;
    debug!("read magic number: {magic}");
    let file_type = f.read_u8().context(ReadTypeSnafu)?;
    debug!("read file type: {file_type}");
    let dimensions = f.read_u8().context(ReadDimensionsSnafu)?;
    debug!("read number of dimensions: {dimensions}");

    ensure!(magic == 0x0000, InvalidMagicSnafu { magic });
    ensure!(
        file_type == TYPE_UNSIGNED_BYTE,
        InvalidTypeSnafu { file_type }
    );
    ensure!(dimensions > 0, ZeroDimensionsSnafu);

    let mut sizes: Vec<usize> = vec![];
    for dimension in 0..dimensions {
        let d = f
            .read_u32::<BigEndian>()
            .context(ReadDimensionSnafu { dimension })?;
        let dimension_size = usize::try_from(d).unwrap_or(0);
        ensure!(
            dimension_size > 0,
            InvalidDimensionSnafu {
                dimension,
                dimension_size
            }
        );
        sizes.push(dimension_size);
        debug!("dimension {dimension} is: {d}");
    }

    let expected_data_length = sizes.iter().product::<usize>();
    let expected_size = expected_data_length + 4 + 4 * sizes.len();
    let meta = f.metadata().context(ReadMetadataSnafu)?;
    let actual_size = usize::try_from(meta.len()).unwrap_or(0);
    ensure!(
        actual_size == expected_size,
        UnexpectedFileSizeSnafu {
            expected_size,
            actual_size
        }
    );

    let mut data: Vec<u8> = Vec::new();
    let bytes_read = f.read_to_end(&mut data).context(ReadDataSnafu)?;
    ensure!(
        bytes_read == expected_data_length,
        DataReadFailedSnafu {
            expected: expected_data_length,
            actual: bytes_read
        }
    );

    Ok(DbFile {
        dimensions,
        sizes,
        data,
    })
}

impl ImageDataset {
    pub fn new() -> ImageDataset {
        ImageDataset {
            rows: 0,
            columns: 0,
            num_items: 0,
            image_buffer: Vec::new(),
            labels: Vec::new(),
            encoding: ImageEncoding::RowMajor,
            item_instances: Vec::new(),
        }
    }

    /// This method obtains an image dataset from the passed files
    pub fn from_file(
        image_file: impl AsRef<Path>,
        label_file: impl AsRef<Path>,
        encoding: ImageEncoding,
    ) -> Result<ImageDataset, Error> {
        let image_db = read_db_file(image_file)?;
        let label_db = read_db_file(label_file)?;

        ensure!(
            image_db.dimensions == IMAGE_DIMENSIONS,
            InvalidImageDimensionsSnafu {
                dimensions: image_db.dimensions
            }
        );
        ensure!(
            label_db.dimensions == LABEL_DIMENSIONS,
            InvalidLabelDimensionsSnafu {
                dimensions: label_db.dimensions
            }
        );

        ensure!(
            image_db.sizes[0] == label_db.sizes[0],
            InvalidDatasetSnafu {
                num_images: image_db.sizes[0],
                num_labels: label_db.sizes[0]
            }
        );

        let mut item_instances: Vec<usize> = Vec::new();
        let max_label = (&label_db.data).into_iter().max().unwrap_or(&0);
        let mut instance_counts = vec![0; (*max_label + 1) as usize];

        // loop through labels getting the instance numbers
        for label in &label_db.data {
            item_instances.push(instance_counts[*label as usize]);
            instance_counts[*label as usize] += 1;
        }

        Ok(ImageDataset {
            rows: image_db.sizes[1],
            columns: image_db.sizes[2],
            num_items: image_db.sizes[0],
            image_buffer: image_db.data,
            labels: label_db.data,
            encoding,
            item_instances,
        })
    }

    /// This method returns an ImageItem from for the passed, zero based, item_index in the data set
    pub fn get_image(&self, item_index: usize) -> Result<ImageItem, Error> {
        ensure!(
            self.num_items > item_index,
            IndexOutOfBoundsSnafu { item_index }
        );
        let mut im = GrayImage::new(self.rows as u32, self.columns as u32);
        let mut pixel_count = 0;
        let image_size = self.rows as usize * self.columns as usize;
        let image_offset = image_size * item_index;
        let image = &self.image_buffer[image_offset..image_offset + image_size];

        for (x, y, pixel) in im.enumerate_pixels_mut() {
            if self.encoding == ImageEncoding::ColumnMajor {
                let pixel_num = x as usize * self.columns + y as usize;
                *pixel = Luma([image[pixel_num]]);
            } else {
                *pixel = Luma([image[pixel_count]]);
                pixel_count += 1;
            }
        }
        Ok(ImageItem {
            label: self.labels[item_index],
            item_index,
            item_instance: self.item_instances[item_index],
            image: im,
        })
    }

    /// Gets an image from the passed data in the same encoding
    pub fn get_image_from_data(&self, data: &[u8]) -> Result<GrayImage, Error> {
        // TODO check for input issues like size

        let mut im = GrayImage::new(self.rows as u32, self.columns as u32);
        let mut pixel_count = 0;
        for (x, y, pixel) in im.enumerate_pixels_mut() {
            if self.encoding == ImageEncoding::ColumnMajor {
                let pixel_num = x as usize * self.columns + y as usize;
                *pixel = Luma([data[pixel_num]]);
            } else {
                *pixel = Luma([data[pixel_count]]);
                pixel_count += 1;
            }
        }

        Ok(im)
    }

    /// Adds an image to the dataset
    pub fn add_image(&mut self, data: &[u8], label: u8) {
        // TODO check the size against the rows * cols

        for p in data {
            self.image_buffer.push(*p);
        }

        self.labels.push(label);

        self.num_items += 1;
    }

    /// This method will obtain all samples matching the passed label
    pub fn get_all_from_label(&self, label: u8) -> Vec<&[u8]> {
        //TODO: do we need to return a result?  can this fail?

        let mut v: Vec<&[u8]> = Vec::new();
        let elt_size = self.rows * self.columns;
        for (i, l) in self.labels.iter().enumerate() {
            if l == &label {
                let offset: usize = i * elt_size;
                v.push(&self.image_buffer[offset..offset + elt_size]);
            }
        }
        v
    }

    pub fn save(
        &self,
        image_file_name: impl AsRef<Path>,
        label_file_name: impl AsRef<Path>,
    ) -> Result<(), Error> {
        let mut im_file = File::create(image_file_name.as_ref()).context(CreateFileSnafu)?;

        // write the image file header

        im_file.write_all(&[0x0, 0x0]).context(WriteDataSnafu)?;
        im_file
            .write_all(&[TYPE_UNSIGNED_BYTE])
            .context(WriteDataSnafu)?;
        im_file.write_all(&[0x3]).context(WriteDataSnafu)?;

        im_file
            .write_all(&(self.num_items as u32).to_be_bytes())
            .context(WriteDataSnafu)?;
        im_file
            .write_all(&(self.columns as u32).to_be_bytes())
            .context(WriteDataSnafu)?;
        im_file
            .write_all(&(self.columns as u32).to_be_bytes())
            .context(WriteDataSnafu)?;

        im_file
            .write_all(&self.image_buffer)
            .context(WriteDataSnafu)?;
        im_file.sync_all().context(WriteDataSnafu)?;

        let mut l_file = File::create(label_file_name.as_ref()).context(CreateFileSnafu)?;

        // write the header

        l_file.write_all(&[0x0, 0x0]).context(WriteDataSnafu)?;
        l_file
            .write_all(&[TYPE_UNSIGNED_BYTE])
            .context(WriteDataSnafu)?;
        l_file.write_all(&[0x1]).context(WriteDataSnafu)?;

        l_file
            .write_all(&(self.num_items as u32).to_be_bytes())
            .context(WriteDataSnafu)?;

        l_file.write_all(&self.labels).context(WriteDataSnafu)?;
        l_file.sync_all().context(WriteDataSnafu)?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::{ImageDataset, ImageEncoding};
    use image::{GrayImage, Luma};
    use tracing::Level;
    use tracing_subscriber::FmtSubscriber;

    // init tracing subscriber
    #[cfg(test)]
    #[ctor::ctor]
    fn init() {
        let _subscriber = FmtSubscriber::builder().with_max_level(Level::TRACE).init();
    }

    #[test]
    #[should_panic(expected = "InvalidDimension")]
    fn read_empty_dataset() {
        let im = ImageDataset::from_file(
            "./assets/image_empty",
            "./assets/label_empty",
            ImageEncoding::RowMajor,
        )
        .unwrap();
    }

    #[test]
    fn read_dataset_with_one_image() {
        let im = ImageDataset::from_file(
            "./assets/image_single",
            "./assets/label_single",
            ImageEncoding::RowMajor,
        )
        .unwrap();

        assert_eq!(im.rows, 1);
        assert_eq!(im.columns, 1);
        assert_eq!(im.num_items, 1);
        assert_eq!(im.image_buffer[0], 0x11);
        assert_eq!(im.labels[0], 0x22);
    }

    #[test]
    fn read_dataset_row_major() {
        let im = ImageDataset::from_file(
            "./assets/image_row_major",
            "./assets/label_row_major",
            ImageEncoding::RowMajor,
        )
        .unwrap();

        assert_eq!(im.rows, 2);
        assert_eq!(im.columns, 3);
        assert_eq!(im.num_items, 2);

        // create golden images
        let mut px_val = 0x01;
        let mut golden_im_01 = GrayImage::new(2, 3);
        for (_x, _y, pixel) in golden_im_01.enumerate_pixels_mut() {
            *pixel = Luma([px_val]);
            px_val += 1;
        }
        let mut golden_im_02 = GrayImage::new(2, 3);
        for (_x, _y, pixel) in golden_im_02.enumerate_pixels_mut() {
            *pixel = Luma([px_val]);
            px_val += 1;
        }

        let image_item_01 = im.get_image(0).expect("Problem getting image 0");
        assert_eq!(image_item_01.image, golden_im_01);
        assert_eq!(image_item_01.item_index, 0);
        assert_eq!(image_item_01.item_instance, 0);
        assert_eq!(image_item_01.label, 1);

        let image_item_02 = im.get_image(1).expect("Problem getting image 1");
        assert_eq!(image_item_02.image, golden_im_02);
        assert_eq!(image_item_02.item_index, 1);
        assert_eq!(image_item_02.item_instance, 0);
        assert_eq!(image_item_02.label, 2);
    }

    #[test]
    fn read_dataset_column_major() {
        let im = ImageDataset::from_file(
            "./assets/image_column_major",
            "./assets/label_column_major",
            ImageEncoding::ColumnMajor,
        )
        .unwrap();

        assert_eq!(im.rows, 2);
        assert_eq!(im.columns, 3);
        assert_eq!(im.num_items, 2);

        // create golden images
        let mut px_val = 0x01;
        let mut golden_im_01 = GrayImage::new(2, 3);
        for (_x, _y, pixel) in golden_im_01.enumerate_pixels_mut() {
            *pixel = Luma([px_val]);
            px_val += 1;
        }
        let mut golden_im_02 = GrayImage::new(2, 3);
        for (_x, _y, pixel) in golden_im_02.enumerate_pixels_mut() {
            *pixel = Luma([px_val]);
            px_val += 1;
        }

        let image_item_01 = im.get_image(0).expect("Problem getting image 0");
        assert_eq!(image_item_01.image, golden_im_01);
        assert_eq!(image_item_01.label, 1);
        assert_eq!(image_item_01.item_index, 0);
        assert_eq!(image_item_01.item_instance, 0);

        let image_item_02 = im.get_image(1).expect("Problem getting image 1");
        assert_eq!(image_item_02.image, golden_im_02);
        assert_eq!(image_item_02.label, 2);
        assert_eq!(image_item_02.item_index, 1);
        assert_eq!(image_item_02.item_instance, 0);
    }

    #[test]
    fn multiple_instances() {
        let im = ImageDataset::from_file(
            "./assets/image_mult_instances",
            "./assets/label_mult_instances",
            ImageEncoding::ColumnMajor,
        )
        .unwrap();

        assert_eq!(im.rows, 1);
        assert_eq!(im.columns, 1);
        assert_eq!(im.num_items, 3);

        let image_item_01 = im.get_image(0).expect("Problem getting image 0");
        assert_eq!(image_item_01.label, 1);
        assert_eq!(image_item_01.item_index, 0);
        assert_eq!(image_item_01.item_instance, 0);

        let image_item_02 = im.get_image(1).expect("Problem getting image 1");
        assert_eq!(image_item_02.label, 2);
        assert_eq!(image_item_02.item_index, 1);
        assert_eq!(image_item_02.item_instance, 0);

        let image_item_03 = im.get_image(2).expect("Problem getting image 1");
        assert_eq!(image_item_03.label, 1);
        assert_eq!(image_item_03.item_index, 2);
        assert_eq!(image_item_03.item_instance, 1);
    }

    #[test]
    #[should_panic(expected = "UnexpectedFileSize")]
    fn read_short_image_file() {
        let _res = ImageDataset::from_file(
            "./assets/image_too_short",
            "./assets/label_single",
            ImageEncoding::RowMajor,
        )
        .unwrap();
    }

    #[test]
    #[should_panic(expected = "InvalidDataset")]
    fn read_different_size_files() {
        let _res = ImageDataset::from_file(
            "./assets/image_row_major",
            "./assets/label_single",
            ImageEncoding::RowMajor,
        )
        .unwrap();
    }

    #[test]
    #[should_panic(expected = "OpenFile")]
    fn read_no_image_file() {
        let _im = ImageDataset::from_file(
            "./assets/not_a_file",
            "./assets/label_empty",
            ImageEncoding::RowMajor,
        )
        .unwrap();
    }

    #[test]
    #[should_panic(expected = "OpenFile")]
    fn read_no_label_file() {
        let _im = ImageDataset::from_file(
            "./assets/image_row_major",
            "./assets/not_a_file",
            ImageEncoding::RowMajor,
        )
        .unwrap();
    }

    #[test]
    #[should_panic(expected = "InvalidImageDimensions")]
    fn read_bad_image_file() {
        let _res = ImageDataset::from_file(
            "./assets/label_single",
            "./assets/label_single",
            ImageEncoding::RowMajor,
        )
        .unwrap();
    }

    #[test]
    #[should_panic(expected = "InvalidLabelDimensions")]
    fn read_bad_label_file() {
        let _res = ImageDataset::from_file(
            "./assets/image_row_major",
            "./assets/image_row_major",
            ImageEncoding::RowMajor,
        )
        .unwrap();
    }
}
